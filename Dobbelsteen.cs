﻿using System;

namespace Vervloessem_wout_oef24._1
{
    class Dobbelsteen
    {
        private int _aantalZijden;
        private int _waarde;
        private Random _willGetal;

        public int AantalZijden { get; set; }
        public int Waarde { get; set; }
        public Random WillGetal { get; set; }

        public Dobbelsteen()
        {
            AantalZijden = 6;
            this.Roll();
        }
        public Dobbelsteen(int aantalZijden, Random r)
        {
            AantalZijden = aantalZijden;
           
            this.Roll();
        }
        public Dobbelsteen(Random r)
        {
            this.Roll();
        }
        public void Roll()
        {
            WillGetal = new Random();
           Waarde=WillGetal.Next(1, AantalZijden);
      

        }
    }
}
