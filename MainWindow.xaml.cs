﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Vervloessem_wout_oef24._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        private void BtnGooien_Click(object sender, RoutedEventArgs e)
        {
            Dobbelsteen Dobbelsteen1 = new Dobbelsteen(4, new Random());
            Dobbelsteen Dobbelsteen2 = new Dobbelsteen();
            tbDobbel1.Text = Dobbelsteen1.Waarde.ToString();
            tbDobbel2.Text = Dobbelsteen2.Waarde.ToString();

        }
    }
}
